
// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// database.ref('expenses').on('child_added', (snapshot) => {
//   console.log(snapshot.key, snapshot.val())
// })

// database.ref('expenses').on('value', (snapshot) => {
//   const expenses = []
//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot
//     })
//   })
//   console.log(expenses)
// }, e => console.log('ERROR with expenses subscription', e))

// database
//   .ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     const expenses = []
//     snapshot.forEach((childSnapshot) => {
//       expenses.push({
//         id: childSnapshot.key,
//         ...childSnapshot
//       })
//     })
//     console.log(expenses)
//   })
//   .catch(e => console.log('ERROR fetching expenses', e))

// const expenses = [
//   {
//     description: 'First',
//     amount: 560,
//     note: '',
//     createdAt: 150
//   },
//   {
//     description: 'Second',
//     amount: 650,
//     note: '',
//     createdAt: 175
//   },
//   {
//     description: 'Third',
//     amount: 720,
//     note: '',
//     createdAt: 195
//   }
// ]

// setTimeout(() => {
//   database.ref('expenses').push(expenses[2])
// }, 3500)

// expenses.forEach(expense => database.ref('expenses').push(expense))

// database.ref('notes/-LBtqSqU45VClUixepSm').remove()

// database.ref('notes').push({
//   title: 'Course Topic',
//   body: 'Interesting stuff'
// })

// const challenge = database.ref().on('value', (snapshot) => {
//   const {name, job: {title, company}} = snapshot.val()
//   console.log(`${name} is a ${title} at ${company}.`)
// }, (e) => {
//   console.log('error on subscribe', e)
// })

// const onValueChange = database.ref()
//   .on('value', (snapshot) => {
//     console.log(snapshot.val())
//   }, (e) => {
//     console.log('error with data fetching', e)
//   })

// setTimeout(() => {
//   database.ref().update({age: 37})
// }, 3500)

// setTimeout(() => {
//   database.ref().off(onValueChange)
// }, 7000)

// setTimeout(() => {
//   database.ref().update({age: 41})
// }, 10500)

// database.ref('location/city')
//   .once('value')
//   .then((snapshot) => {
//     const val = snapshot.val()
//     console.log(val)
//   })
//   .catch(e => console.log('fetching failed', e))

// database.ref().set({
//   name: 'Zoli',
//   age: 30,
//   stressLevel: 6,
//   job: {
//     title: 'Web Developer',
//     company: 'Google'
//   },
//   location: {
//     city: 'Godollo',
//     country: 'Remote Country'
//   }
// }).then(() => {
//   console.log('data in')
// }).catch((e) => {
//   console.log('failed', e)
// })

// database.ref().remove().then(() => { console.log('shite remove') }).catch((e) => {
//   console.log('failed', e)
// })

// database.ref().update({
//   stressLevel: 9,
//   'location/city': 'Seattle',
//   'job/company': 'Amazon'
// }).then(() => { console.log('updated') })
