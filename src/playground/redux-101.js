import {createStore} from 'redux'
import watch from 'redux-watch'

const add = ({a, b}, c) => {
  return a + b + c
}

console.log(add({a: 5, b: 6}, 9))

const incrementCount = ({incrementBy = 1} = {}) => ({
  type: 'INCREMENT',
  incrementBy
})

const decrementCount = ({decrementBy = 1} = {}) => ({
  type: 'DECREMENT',
  decrementBy
})

const setCount = ({count}) => ({
  type: 'SET',
  count
})

const resetCount = () => ({type: 'RESET'})

// Reducer
// 1. Output is only determined by the input = pure function
// 2. Never change state or action directly

const countReducer = (state = {count: 0}, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        count: state.count + action.incrementBy
      }
    case 'DECREMENT':
      return {
        count: state.count - action.decrementBy
      }
    case 'SET':
      return {
        count: action.count
      }
    case 'RESET':
      return {
        count: 0
      }
    default:
      return state
  }
}

const store = createStore(
  countReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

let w = watch(store.getState, 'count')
store.subscribe(w((newVal, oldVal, objectPath) => {
  console.log('%s changed from %s to %s', objectPath, oldVal, newVal)
}))

// store.dispatch({
//   type: 'INCREMENT',
//   incrementBy: 5
// })

store.dispatch(incrementCount({incrementBy: 15}))

store.dispatch(incrementCount())

store.dispatch(decrementCount({decrementBy: 2}))

store.dispatch(decrementCount())

store.dispatch(resetCount())

store.dispatch(setCount({count: 101}))
