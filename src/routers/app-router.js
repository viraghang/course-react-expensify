import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'

// Component imports
import ExpenseDashboardPage from '../components/expense-dashboard'
import AddExpensePage from '../components/add-expense'
import EditExpensePage from '../components/edit-expense'
import HelpPage from '../components/help'
import NotFoundPage from '../components/404'
import Header from '../components/header'

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route
          path='/'
          component={ExpenseDashboardPage}
          exact
        />
        <Route
          path='/create'
          component={AddExpensePage}
        />
        <Route
          path='/edit/:id'
          component={EditExpensePage}
        />
        <Route
          path='/help'
          component={HelpPage}
        />
        <Route
          component={NotFoundPage}
        />
      </Switch>
    </div>
  </BrowserRouter>
)

export default AppRouter
