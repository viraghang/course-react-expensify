/* Third-party libraries */
import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import AppRouter from './routers/app-router'
import configureStore from './store/configureStore'
import { startSetExpenses } from './actions/expenses'
import './firebase/firebase'
// import getVisibleExpenses from './selectors/expenses'
import 'react-dates/initialize'
import 'normalize.css/normalize.css'
import 'react-dates/lib/css/_datepicker.css'
import './styles/styles.styl'
// import './playground/promises'

const store = configureStore()

// store.subscribe(() => {
//   const state = store.getState()
//   const visibleExpenses = getVisibleExpenses(state.expenses, state.filters)
//   console.log(visibleExpenses, state.filters)
// })

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
)

ReactDOM.render(<p>Loading...</p>, document.getElementById('app'))

store.dispatch(startSetExpenses()).then(() => {
  // Rendering to the DOM
  ReactDOM.render(jsx, document.getElementById('app'))
})
