export default [
  {
    id: '1',
    description: 'Malacka',
    amount: 5000,
    note: 'Oink oink',
    createdAt: 1524679400712
  },
  {
    id: '2',
    description: 'Tigris',
    amount: 50000,
    note: 'Roar',
    createdAt: 1525025000712
  },
  {
    id: '3',
    description: 'Lobelt Gida',
    amount: 3000,
    note: 'Foolish fellow',
    createdAt: 1525111400712
  }
]
