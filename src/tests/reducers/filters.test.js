/* globals test, expect */

import filtersReducer from '../../reducers/filters'
import moment from 'moment'

const filtersReducerDefaultState = {
  text: '',
  sortBy: 'date',
  startDate: moment().startOf('month'),
  endDate: moment().endOf('month')
}

test('should setup default filter values', () => {
  const state = filtersReducer(undefined, {type: '@@INIT'})
  expect(state).toEqual({
    ...filtersReducerDefaultState
  })
})

test('should set sortBy to amount', () => {
  const state = filtersReducer(undefined, {type: 'SORT_BY_AMOUNT'})
  expect(state.sortBy).toBe('amount')
})

test('should set sortBy to date', () => {
  const currentState = {
    ...filtersReducerDefaultState,
    sortBy: 'amount'
  }
  const action = {type: 'SORT_BY_DATE'}
  const state = filtersReducer(currentState, action)
  expect(state.sortBy).toBe('date')
})

test('should set text filter', () => {
  const action = {type: 'SET_TEXT_FILTER', text: 'Ebbol meg sok van?'}
  const state = filtersReducer(undefined, action)
  expect(state.text).toBe('Ebbol meg sok van?')
})

test('should set startDate filter', () => {
  const action = {type: 'SET_START_DATE', date: moment(0)}
  const state = filtersReducer(undefined, action)
  expect(state.startDate).toEqual(moment(0))
})

test('should set endDate filter', () => {
  const action = {type: 'SET_END_DATE', date: moment(0).add(30, 'years')}
  const state = filtersReducer(undefined, action)
  expect(state.endDate).toEqual(moment(0).add(30, 'years'))
})
