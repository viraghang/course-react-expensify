/* globals test, expect */

import selectExpenses from '../../selectors/expenses'
import expenses from '../fixtures/expenses'
import moment from 'moment'

const filters = {
  text: '',
  sortBy: 'date',
  startDate: undefined,
  endDate: undefined
}

test('should filter by text value', () => {
  const filtersVariant = {
    ...filters,
    text: 'a'
  }
  const result = selectExpenses(expenses, filtersVariant)
  expect(result).toEqual([expenses[2], expenses[0]])
})

test('should filter by startDate', () => {
  const filtersVariant = {
    ...filters,
    startDate: moment(1525111400700)
  }
  const result = selectExpenses(expenses, filtersVariant)
  expect(result).toEqual([expenses[2]])
})

test('should filter by endDate', () => {
  const filtersVariant = {
    ...filters,
    endDate: moment(1524679400720)
  }
  const result = selectExpenses(expenses, filtersVariant)
  expect(result).toEqual([expenses[0]])
})

test('should sort by date', () => {
  const result = selectExpenses(expenses, filters)
  expect(result).toEqual([expenses[2], expenses[1], expenses[0]])
})

test('should sort by amount', () => {
  const filtersVariant = {
    ...filters,
    sortBy: 'amount'
  }
  const result = selectExpenses(expenses, filtersVariant)
  expect(result).toEqual([expenses[1], expenses[0], expenses[2]])
})
