/* globals test, expect */

import { sortByAmount, sortByDate, setEndDate, setStartDate, setTextFilter } from '../../actions/filters'
import moment from 'moment'

test('should generate set start date action object', () => {
  const action = setStartDate(moment(0))
  expect(action).toEqual({
    type: 'SET_START_DATE',
    date: moment(0)
  })
})

test('should generate set end date action object', () => {
  const action = setEndDate(moment(0))
  expect(action).toEqual({
    type: 'SET_END_DATE',
    date: moment(0)
  })
})

test('should generate text filter action object with the provided data', () => {
  const action = setTextFilter('majom')
  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: 'majom'
  })
})

test('should generate text filter action object with default data', () => {
  const action = setTextFilter()
  expect(action).toEqual({
    type: 'SET_TEXT_FILTER',
    text: ''
  })
})

test('should generate sortBy action object with sorting set to amount', () => {
  expect(sortByAmount()).toEqual({ type: 'SORT_BY_AMOUNT' })
})

test('should generate sortBy action object with sorting set to date', () => {
  expect(sortByDate()).toEqual({ type: 'SORT_BY_DATE' })
})
