/* globals test, expect */
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { startAddExpense, addExpense, editExpense, removeExpense, setExpenses, startSetExpenses, startRemoveExpense, startEditExpense } from '../../actions/expenses'
import expenses from '../fixtures/expenses'
import database from '../../firebase/firebase'

const createMockStore = configureMockStore([thunk])

beforeEach((done) => {
  const expensesData = {}
  expenses.forEach(({id, description, amount, createdAt, note}) => {
    expensesData[id] = { description, amount, createdAt, note }
  })
  database.ref('expenses').set(expensesData).then(() => done())
})

test('should setup remove expense action object', () => {
  const action = removeExpense({id: '123abc'})
  expect(action).toEqual({
    type: 'REMOVE_EXPENSE',
    id: '123abc'
  })
})

test('should setup edit expense action object', () => {
  const action = editExpense('123abc', {description: 'Kutya', amount: 1, createdAt: 553435534})
  expect(action).toEqual({
    type: 'EDIT_EXPENSE',
    id: '123abc',
    updates: {
      description: 'Kutya',
      amount: 1,
      createdAt: 553435534
    }
  })
})

test('should setup addExpense action object with provided values', () => {
  const action = addExpense(expenses[2])
  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: expenses[2]
  })
})

test('should add expense to database and store', (done) => {
  const store = createMockStore({})
  const expenseData = {
    description: 'Mouse',
    amount: 3000,
    note: 'This one is better',
    createdAt: 1000
  }

  store.dispatch(startAddExpense(expenseData)).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseData
      }
    })
    return database.ref(`expenses/${actions[0].expense.id}`).once('value')
  }).then((snapshot) => {
    expect(snapshot.val()).toEqual(expenseData)
    done()
  })
})

test('should add expense with defaults to database and store', (done) => {
  const store = createMockStore({})
  const expenseData = {
    description: '',
    amount: 0,
    note: '',
    createdAt: 0
  }

  store.dispatch(startAddExpense({})).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'ADD_EXPENSE',
      expense: {
        id: expect.any(String),
        ...expenseData
      }
    })
    return database.ref(`expenses/${actions[0].expense.id}`).once('value')
  }).then((snapshot) => {
    expect(snapshot.val()).toEqual(expenseData)
    done()
  })
})

test('should setup set expense action object with data', () => {
  const action = setExpenses(expenses)
  expect(action).toEqual({ type: 'SET_EXPENSES', expenses })
})

test('should fetch the expenses from FB', (done) => {
  const store = createMockStore([])
  store.dispatch(startSetExpenses()).then((returnedExpenses) => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'SET_EXPENSES',
      expenses
    })
    done()
  })
})

test('should remove expense from FB', (done) => {
  const store = createMockStore(expenses)
  const {id: itemId} = expenses[1]
  store.dispatch(startRemoveExpense(itemId)).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'REMOVE_EXPENSE',
      id: itemId
    })

    return database.ref(`expenses/${itemId}`).once('value')
  }).then((snapshot) => {
    expect(snapshot.val()).toBe(null)
    done()
  })
})

test('should edit expense in Firebase', (done) => {
  const expense = expenses[2]
  const updates = { description: 'Robert Gida' }
  const store = createMockStore([expense])
  store.dispatch(startEditExpense(expense.id, updates)).then(() => {
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: 'EDIT_EXPENSE',
      id: expense.id,
      updates
    })

    return database.ref(`expenses/${expense.id}`).once('value')
  }).then((snapshot) => {
    expect(snapshot.val().description).toBe(updates.description)
    done()
  })
})
