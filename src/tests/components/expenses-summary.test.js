import React from 'react'
import { shallow } from 'enzyme'
import { ExpensesSummary } from '../../components/expenses-summary'
import getExpensesTotal from '../../selectors/expenses-total'
import numeral from 'numeral'
import expenses from '../fixtures/expenses'

test('should render correctly with 1 expense', () => {
  const expenseTotal = numeral(getExpensesTotal([expenses[1]]) / 100).format('$0,0.00')
  const wrapper = shallow(<ExpensesSummary expenseCount={1} expenseTotal={expenseTotal} />)
  expect(wrapper).toMatchSnapshot()
})

test('should render correctly with 0 or multiple expenses', () => {
  const expenseTotal = numeral(getExpensesTotal(expenses) / 100).format('$0,0.00')
  const wrapper = shallow(<ExpensesSummary expenseCount={expenses.length} expenseTotal={expenseTotal} />)
  expect(wrapper).toMatchSnapshot()
})
