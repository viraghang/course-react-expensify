/* globals expect, test */

import React from 'react'
import { shallow } from 'enzyme'
import ExpenseDashboardPage from '../../components/expense-dashboard'

test('should render the ExpenseDashboardPage', () => {
  const wrapper = shallow(<ExpenseDashboardPage />)
  expect(wrapper).toMatchSnapshot()
})
