/* globals test, expect */

import React from 'react'
import { shallow } from 'enzyme'
import ExpenseListItem from '../../components/expense-list-item'
import expenses from '../fixtures/expenses'

test('should render expense list item wit expense', () => {
  const wrapper = shallow(<ExpenseListItem key={expenses[1].id} {...expenses[1]} />)
  expect(wrapper).toMatchSnapshot()
})
