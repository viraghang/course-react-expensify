import React from 'react'
import { connect } from 'react-redux'
import getExpensesTotal from '../selectors/expenses-total'
import selectExpense from '../selectors/expenses'
import numeral from 'numeral'

export const ExpensesSummary = ({expenseCount, expenseTotal}) => {
  const expenseWord = expenseCount === 1 ? 'expense' : 'expenses'
  return (
    <div>
      <p>{`Viewing ${expenseCount} ${expenseWord} totalling ${expenseTotal}`}</p>
    </div>
  )
}

const mapStateToProps = (state) => {
  const visibleExpenses = selectExpense(state.expenses, state.filters)
  return {
    expenseTotal: numeral(getExpensesTotal(visibleExpenses) / 100).format('$0,0.00'),
    expenseCount: visibleExpenses.length
  }
}

export default connect(mapStateToProps)(ExpensesSummary)
